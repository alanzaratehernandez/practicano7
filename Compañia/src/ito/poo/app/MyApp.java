package ito.poo.app;
import ito.poo.clases.Maquina;
import ito.poo.clases.MLavado;
import ito.poo.clases.MLlenadoEnvasado;
import ito.poo.clases.MEmpaquetado;
import java.time.LocalDate;

public class MyApp {
	
	static void run() {
		Maquina m= new Maquina(LocalDate.of(2021,10,10),1500);
		System.out.println(m);
		MLavado ml= new MLavado(LocalDate.of(2021,10,10),1500,13,3);
		System.out.println(ml);
		MLlenadoEnvasado mle= new MLlenadoEnvasado(LocalDate.of(2021,10,10),1500,5,5);
		System.out.println(mle);
		MEmpaquetado me= new MEmpaquetado(LocalDate.of(2021,10,10),1500,2,3);
		System.out.println(me);
		
		System.out.println("Costo de lavado="+ml.CostoLavado(1500));
		System.out.println("Costo de llenado y envasado="+mle.CostoLlenadoenvasado(5000));
		System.out.println("Costo de empaquetado="+me.CostoEmpaquetado(8500));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        run();
	}

}
