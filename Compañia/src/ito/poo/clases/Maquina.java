package ito.poo.clases;
import java.time.LocalDate;

public class Maquina {
	
	private LocalDate FechaAd;
	private float Costo;
	
	public Maquina(LocalDate FechaAd, float Costo) {
		super();
		this.FechaAd=FechaAd;
		this.Costo=Costo;
	}

	public LocalDate getFechaAd() {
		return FechaAd;
	}

	public void setFechaAd(LocalDate fechaAd) {
		FechaAd = fechaAd;
	}

	public float getCosto() {
		return Costo;
	}

	public void setCosto(float costo) {
		Costo = costo;
	}

	@Override
	public String toString() {
		return "Maquina [FechaAd=" + FechaAd + ", Costo=" + Costo + "]";
	}
}
