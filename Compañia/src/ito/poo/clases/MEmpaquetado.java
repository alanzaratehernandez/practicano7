package ito.poo.clases;
import java.time.LocalDate;

public class MEmpaquetado extends Maquina {
	private int Tipo;
	private int CantxMin;
	
	public MEmpaquetado(LocalDate FechaAd, float Costo, int Tipo, int CantxMin) {
		super(FechaAd,Costo);
		this.Tipo=Tipo;
		this.CantxMin=CantxMin;
	}
	
	public float CostoEmpaquetado(float costob) {
		return (costob*0.06f)/27;
	}

	public int getTipo() {
		return Tipo;
	}

	public void setTipo(int tipo) {
		Tipo = tipo;
	}

	public int getCantxMin() {
		return CantxMin;
	}

	public void setCantxMin(int cantxMin) {
		CantxMin = cantxMin;
	}

	@Override
	public String toString() {
		return "MEmpaquetado [Tipo=" + Tipo + ", CantxMin=" + CantxMin + "]";
	}
}
