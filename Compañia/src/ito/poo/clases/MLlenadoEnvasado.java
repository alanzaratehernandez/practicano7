package ito.poo.clases;
import java.time.LocalDate;

public class MLlenadoEnvasado extends Maquina {
	private int CantEnvasxM;
	private int ListaRegulacionMili;
	
	public MLlenadoEnvasado(LocalDate FechaAd, float Costo, int CantEnvasxM, int ListaRegulacionMili) {
		super(FechaAd,Costo);
		this.CantEnvasxM=CantEnvasxM;
		this.ListaRegulacionMili=ListaRegulacionMili;
	}
	
	public float CostoLlenadoenvasado(float costob) {
		return (costob*0.25f)/15;
	}

	public int getCantEnvasxM() {
		return CantEnvasxM;
	}

	public void setCantEnvasxM(int cantEnvasxM) {
		CantEnvasxM = cantEnvasxM;
	}

	public int getListaRegulacionMili() {
		return ListaRegulacionMili;
	}

	public void setListaRegulacionMili(int listaRegulacionMili) {
		ListaRegulacionMili = listaRegulacionMili;
	}

	@Override
	public String toString() {
		return "MLlenadoEnvasado [CantEnvasxM=" + CantEnvasxM + ", ListaRegulacionMili=" + ListaRegulacionMili + "]";
	}
}
